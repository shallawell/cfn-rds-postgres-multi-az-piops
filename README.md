# Summary

## AWS cloudformation template to create a RDS Postgres stack.

* input parameters
* output parameters
* RDS instance, engine type and version
* multi az (change to true to enable)
* provisioned iops
* RDS instance tags
* Delete protection (will retain snapshot after RDS is deleted)

## AWS cloudformation template to create a RDS Security Group stack.

* 2 security groups
* 1 for web (allows 22, 80, 443 in, 5432 out to RDS-SecGroup, ANY out to 0.0.0.0/0)
* 1 for RDS (allows 5432 in, ANY out)
* Security Group tags
* output parameters
* input parameters - required a VPC to be defined.


## Create stack example awscli command
aws cloudformation create-stack --stack-name myPostgresRDSstack \
  --template-url https://s3-ap-southeast-2.amazonaws.com/<BUCKETNAME>/rds-multi-AZ-piops-with-input-output-secGrps.json \
  --parameters  ParameterKey=MyDBClass,ParameterValue=db.t2.micro \
  ParameterKey=MyDBUsername,ParameterValue=root \
  ParameterKey=MyDBPassword,ParameterValue=Welcome1 \
  ParameterKey=MyDBName,ParameterValue=MyPgDB

## Update stack example awscli command
aws cloudformation update-stack --stack-name myPostgresRDSstack  \
  --template-url https://s3-ap-southeast-2.amazonaws.com/<BUCKETNAME>/try1.json  \
  --parameters  ParameterKey=MyDBUsername,ParameterValue=root 
  ParameterKey=MyDBPassword,ParameterValue=Welcome1

Hint: MyDBUsername and MyDBPassword are required to update stack

## Cfn coding help

use cfn-flip (pip install cfn-flip) to switch between json-yaml-json

## Credits

http://www.stojanveselinovski.com/blog/2016/01/12/simple-postgresql-rds-cloudformation-template/ 
